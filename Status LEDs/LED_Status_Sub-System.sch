EESchema Schematic File Version 4
LIBS:Pi HAT UPS-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title "LED Status subsystem for rpi uHAT UPS "
Date "2021-06-04"
Rev "0.1"
Comp "Group 5"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R_US R3
U 1 1 60BCA72C
P 5900 2700
F 0 "R3" V 5695 2700 50  0000 C CNN
F 1 "500" V 5786 2700 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 5940 2690 50  0001 C CNN
F 3 "~" H 5900 2700 50  0001 C CNN
	1    5900 2700
	0    1    1    0   
$EndComp
$Comp
L Device:R_US R2
U 1 1 60BCD659
P 4200 3000
F 0 "R2" H 4268 3046 50  0000 L CNN
F 1 "100" H 4268 2955 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 4240 2990 50  0001 C CNN
F 3 "~" H 4200 3000 50  0001 C CNN
	1    4200 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 2900 4200 2850
Wire Wire Line
	4200 2700 5000 2700
Connection ~ 4200 2850
Wire Wire Line
	4200 2850 4200 2700
Connection ~ 5750 2700
Wire Wire Line
	5750 2700 5800 2700
Wire Wire Line
	5900 3700 6800 3700
Wire Wire Line
	6000 2700 6050 2700
Wire Wire Line
	6800 2700 6800 3700
Connection ~ 6050 2700
Wire Wire Line
	6050 2700 6800 2700
Connection ~ 6800 3700
Wire Wire Line
	6800 3700 8000 3700
Wire Wire Line
	5300 3600 5000 3600
Wire Wire Line
	5000 3600 5000 2700
Connection ~ 5000 2700
Wire Wire Line
	5000 2700 5750 2700
Wire Wire Line
	5300 3800 3400 3800
$Comp
L power:GND #PWR01
U 1 1 60BCF35E
P 4200 3300
F 0 "#PWR01" H 4200 3050 50  0001 C CNN
F 1 "GND" H 4205 3127 50  0000 C CNN
F 2 "" H 4200 3300 50  0001 C CNN
F 3 "" H 4200 3300 50  0001 C CNN
	1    4200 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 3300 4200 3150
Connection ~ 4200 3150
Wire Wire Line
	4200 3150 4200 3100
Text GLabel 3400 3800 2    50   Input ~ 0
Vreg
$Comp
L Amplifier_Operational:AD8603 U1
U 1 1 60BCAFDA
P 5600 3700
F 0 "U1" H 5944 3746 50  0000 L CNN
F 1 "AD8603" H 5944 3655 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TSOT-23-5" H 5600 3700 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/AD8603_8607_8609.pdf" H 5600 3900 50  0001 C CNN
	1    5600 3700
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0104
U 1 1 60BEAC06
P 5500 3400
F 0 "#FLG0104" H 5500 3475 50  0001 C CNN
F 1 "PWR_FLAG" H 5500 3573 50  0000 C CNN
F 2 "" H 5500 3400 50  0001 C CNN
F 3 "~" H 5500 3400 50  0001 C CNN
	1    5500 3400
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0105
U 1 1 60BEB4AC
P 5500 4000
F 0 "#FLG0105" H 5500 4075 50  0001 C CNN
F 1 "PWR_FLAG" H 5500 4173 50  0000 C CNN
F 2 "" H 5500 4000 50  0001 C CNN
F 3 "~" H 5500 4000 50  0001 C CNN
	1    5500 4000
	1    0    0    -1  
$EndComp
Text HLabel 8000 3700 2    50   Output ~ 0
Vout_opamp
$EndSCHEMATC
